#include <stdint.h>

#pragma once

class Host
{
public:
  virtual void init(int buzzerPin)=0;
  virtual void sleep(long ms)=0;
  virtual void digitalWrite(int pin,int state)=0;
  virtual int digitalRead(int pin)=0;
  virtual int analogRead(int pin)=0;
  virtual void pinMode(int pin, int mode)=0;
  virtual void click()=0;
  virtual void startupTone()=0;
  virtual void cls()=0;
  virtual void showBuffer()=0;
  virtual void moveCursor(int x, int y)=0;
  virtual void outputString(const char *str)=0;
  virtual void outputChar(char c)=0;
  virtual void outputFloat(float f)=0;
  virtual char *floatToStr(float f, char *buf)=0;
  virtual void outputInt(long val)=0;
  virtual void newLine()=0;
  virtual char *readLine()=0;
  virtual char getKey()=0;
  virtual bool ESCPressed()=0;
  virtual void outputFreeMem(unsigned int val)=0;
  virtual void saveProgram(bool autoexec)=0;
  virtual void loadProgram()=0;
  virtual int getMillis()=0;

  #if EXTERNAL_EEPROM
  #include <I2cMaster.h>
  virtual void writeExtEEPROM(unsigned int address, byte data)=0;
  virtual void directoryExtEEPROM()=0;
  virtual bool saveExtEEPROM(char *fileName)=0;
  virtual bool loadExtEEPROM(char *fileName)=0;
  virtual bool removeExtEEPROM(char *fileName)=0;
  #endif
};
