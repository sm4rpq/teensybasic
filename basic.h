#pragma once

#include <stdint.h>
#include "host.h"

#define TOKEN_EOL		             0
#define TOKEN_IDENT		           1	// special case - identifier follows
#define TOKEN_INTEGER	           2	// special case - integer follows (line numbers only)
#define TOKEN_NUMBER	           3	// special case - number follows
#define TOKEN_STRING	           4	// special case - string follows

#define TOKEN_LBRACKET	         8
#define TOKEN_RBRACKET	         9
#define TOKEN_PLUS	    	      10
#define TOKEN_MINUS		          11
#define TOKEN_MULT		          12
#define TOKEN_DIV		            13
#define TOKEN_PWR               14
#define TOKEN_EQUALS	          15
#define TOKEN_GT		            16
#define TOKEN_LT		            17
#define TOKEN_NOT_EQ	          18
#define TOKEN_GT_EQ		          19
#define TOKEN_LT_EQ		          20
#define TOKEN_CMD_SEP	          21
#define TOKEN_SEMICOLON	        22
#define TOKEN_COMMA		          23
#define TOKEN_AND		            24	// FIRST_IDENT_TOKEN
#define TOKEN_OR		            25
#define TOKEN_NOT		            26
#define TOKEN_PRINT		          27
#define TOKEN_LET		            28
#define TOKEN_LIST		          29
#define TOKEN_RUN		            30
#define TOKEN_GOTO		          31
#define TOKEN_REM		            32
#define TOKEN_STOP		          33
#define TOKEN_INPUT	            34
#define TOKEN_CONT		          35
#define TOKEN_IF		            36
#define TOKEN_THEN		          37
#define TOKEN_LEN		            38
#define TOKEN_VAL		            39
#define TOKEN_RND		            40
#define TOKEN_INT		            41
#define TOKEN_STR		            42
#define TOKEN_FOR		            43
#define TOKEN_TO		            44
#define TOKEN_STEP		          45
#define TOKEN_NEXT		          46
#define TOKEN_MOD		            47
#define TOKEN_NEW		            48
#define TOKEN_GOSUB		          49
#define TOKEN_RETURN	          50
#define TOKEN_DIM		            51
#define TOKEN_LEFT		          52
#define TOKEN_RIGHT	            53
#define TOKEN_MID		            54
#define TOKEN_CLS               55
#define TOKEN_PAUSE             56
#define TOKEN_POSITION          57
#define TOKEN_PIN               58
#define TOKEN_PINMODE           59
#define TOKEN_INKEY             60
#define TOKEN_SAVE              61
#define TOKEN_LOAD              62
#define TOKEN_PINREAD           63
#define TOKEN_ANALOGRD          64
#define TOKEN_DIR               65
#define TOKEN_DELETE            66
#define TOKEN_SIN               67
#define TOKEN_COS               68
#define TOKEN_ATN               69
#define TOKEN_SQRT              70
#define TOKEN_LN                71
#define TOKEN_EXP               72
#define TOKEN_ABS               73
#define TOKEN_MILLIS            74
#define TOKEN_ACOS              75
#define TOKEN_ASIN              76
#define TOKEN_SGN               77
#define TOKEN_ATAN2             78
#define TOKEN_CHR               79
#define TOKEN_ASC               80
#define TOKEN_END               81

#define FIRST_NON_ALPHA_TOKEN    8
#define LAST_NON_ALPHA_TOKEN    23

#define FIRST_IDENT_TOKEN       24
#define LAST_IDENT_TOKEN        81


#define ERROR_NONE				0
// parse errors
#define ERROR_LEXER_BAD_NUM			1
#define ERROR_LEXER_TOO_LONG			2
#define ERROR_LEXER_UNEXPECTED_INPUT	        3
#define ERROR_LEXER_UNTERMINATED_STRING         4
#define ERROR_EXPR_MISSING_BRACKET		5
#define ERROR_UNEXPECTED_TOKEN			6
#define ERROR_EXPR_EXPECTED_NUM			7
#define ERROR_EXPR_EXPECTED_STR			8
#define ERROR_LINE_NUM_TOO_BIG			9
// runtime errors
#define ERROR_OUT_OF_MEMORY			10
#define ERROR_EXPR_DIV_ZERO			11
#define ERROR_VARIABLE_NOT_FOUND		12
#define ERROR_UNEXPECTED_CMD			13
#define ERROR_BAD_LINE_NUM			14
#define ERROR_BREAK_PRESSED			15
#define ERROR_NEXT_WITHOUT_FOR			16
#define ERROR_STOP_STATEMENT			17
#define ERROR_MISSING_THEN			18
#define ERROR_RETURN_WITHOUT_GOSUB		19
#define ERROR_WRONG_ARRAY_DIMENSIONS	        20
#define ERROR_ARRAY_SUBSCRIPT_OUT_RANGE	        21
#define ERROR_STR_SUBSCRIPT_OUT_RANGE	        22
#define ERROR_IN_VAL_INPUT			23
#define ERROR_BAD_PARAMETER                     24

#define MAX_IDENT_LEN	8
#define MAX_NUMBER_LEN	10

#define MEMORY_SIZE	32768
extern unsigned char mem[];
extern int sysPROGEND;
extern int sysSTACKSTART;
extern int sysSTACKEND;
extern int sysVARSTART;
extern int sysVAREND;
extern int sysGOSUBSTART;
extern int sysGOSUBEND;

extern uint16_t lineNumber;	// 0 = input buffer

typedef struct {
    float val;
    float step;
    float end;
    uint16_t lineNumber;
    uint16_t stmtNumber;
} 
ForNextData;

typedef struct {
    char *token;
    uint8_t format;
} 
TokenTableEntry;

//extern const char *errorTable[];
extern const char* const errorTable[];

void reset();
int tokenize(unsigned char *input, unsigned char *output, int outputSize);
int processInput(unsigned char *tokenBuf);
void basic_init(Host* host);

