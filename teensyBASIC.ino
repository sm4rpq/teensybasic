#include <EEPROM.h>
#include <vt_driver.h>

#include "basic.h"
#include "serial_host.h"

// Define in host.h if using an external EEPROM e.g. 24LC256
// Should be connected to the I2C pins
// SDA -> Analog Pin 4, SCL -> Analog Pin 5
// See e.g. http://www.hobbytronics.co.uk/arduino-external-eeprom

// If using an external EEPROM, you'll also have to initialise it by
// running once with the appropriate lines enabled in setup() - see below

#if EXTERNAL_EEPROM
#include <I2cMaster.h>
// Instance of class for hardware master with pullups enabled
TwiMaster rtc(true);
#endif

SerialHost host;

// I/O vs terminal
vtDriver vt100(&Serial1,81);

// buzzer pin, 0 = disabled/not present
#define BUZZER_PIN    0

// BASIC
unsigned char mem[MEMORY_SIZE];
#define TOKEN_BUF_SIZE    64
unsigned char tokenBuf[TOKEN_BUF_SIZE];

const char welcomeStr[] = "Arduino BASIC for Teensy 3";
char autorun = 0;

void setup() {
    Serial1.begin(9600);
    pinMode(13,OUTPUT);
    basic_init(&host);
    reset();
    host.init(BUZZER_PIN);
    host.cls();
    host.outputString(welcomeStr);
    // show memory size
    host.outputFreeMem(sysVARSTART - sysPROGEND);
    
    // IF USING EXTERNAL EEPROM
    // The following line 'wipes' the external EEPROM and prepares
    // it for use. Uncomment it, upload the sketch, then comment it back
    // in again and upload again, if you use a new EEPROM.
    // writeExtEEPROM(0,0); writeExtEEPROM(1,0);

    if (EEPROM.read(0) == MAGIC_AUTORUN_NUMBER)
        autorun = 1;
    else
        host.startupTone();
}

void loop() {
    int ret = ERROR_NONE;

    if (!autorun) {
        // get a line from the user
        host.newLine();
        char *input = host.readLine();
        // special editor commands
        if (input[0] == '?' && input[1] == 0) {
            host.outputFreeMem(sysVARSTART - sysPROGEND);
            return;
        }
        // otherwise tokenize
        ret = tokenize((unsigned char*)input, tokenBuf, TOKEN_BUF_SIZE);
    } else {
        host.loadProgram();
        tokenBuf[0] = TOKEN_RUN;
        tokenBuf[1] = 0;
        autorun = 0;
    }
    // execute the token buffer
    if (ret == ERROR_NONE) {
        host.newLine();
        digitalWrite(13,HIGH);
        ret = processInput(tokenBuf);
        digitalWrite(13,LOW);
    }
    if (ret != ERROR_NONE) {
        host.newLine();
        if (lineNumber !=0) {
            host.outputInt(lineNumber);
            host.outputChar('-');
        }
        host.outputString((char *)(errorTable[ret]));
    }
}

