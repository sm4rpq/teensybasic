﻿Arduino Basic
=============
Now you can turn your Teensy into an 70's home computer!

A complete BASIC interpreter for the Teensy, using a serial port for input and output. The BASIC supports almost all the usual features, with float and string variables, multi-dimensional arrays, FOR-NEXT, GOSUB-RETURN, etc.

Saving and Loading from EEPROM was support by the ArduinoBasic this if forked from but due to memory size difference it is currently probably not working.

You can also read and write from the analog and digital pins.

Programs are stored in a fixed size buffer set to 32k like what you could find on any good old mainframe. As stated earlier, save can be a problem.

Please read the information in the project I forked this from:
https://github.com/robinhedwards/ArduinoBASIC


BASIC Language
--------------
Variables names can be up to 8 alphanumeric characters but start with a letter e.g. a, bob32
String variable names must end in $ e.g. a$, bob32$
Case is ignored (for all identifiers). BOB32 is the same as Bob32. print is the same as PRINT

Array variables are independent from normal variables. So you can use both:
```
LET a = 5
DIM a(10)
```
There is no ambiguity, since a on its own refers to the simple variable 'a', and a(n) referes to an element of the 'a' array.

```
Arithmetic operators: + - * / MOD ^
Comparisons: <> (not equals) > >= < <=
Logical: AND, OR, NOT
```

Expressions can be numerical e.g. 5*(3+2), or string "Hello "+"world".
Only the addition operator is supported on strings (plus the functions below).

Commands
```
PRINT <expr>;<expr>... e.g. PRINT "A=";a
LET variable = <expr> e.g. LET A$="Hello".
variable = <expr> e.g. A=5
LIST [start],[end] e.g. LIST or LIST 10,100
RUN [lineNumber]
GOTO lineNumber
REM <comment> e.g. REM ** My Program ***
STOP
CONT (continue from a STOP)
INPUT variable e.g. INPUT a$ or INPUT a(5,3)
IF <expr> THEN cmd e.g. IF a>10 THEN a = 0: GOTO 20
FOR variable = start TO end STEP step
NEXT variable
NEW
GOSUB lineNumber
RETURN
DIM variable(n1,n2...)
CLS
PAUSE milliseconds
POSITION x,y sets the cursor
PIN pinNum, value (0 = low, non-zero = high)
PINMODE pinNum, mode ( 0 = input, 1 = output)
LOAD (from internal EEPROM)
SAVE (to internal EEPROM) e.g. use SAVE + to set auto-run on boot flag
LOAD "filename", SAVE "filename, DIR, DELETE "filename" if using with external EEPROM.
```

"Pseudo-identifiers"
```
INKEY$ - returns (and eats) the last key pressed buffer (non-blocking). e.g. PRINT INKEY$
RND - random number betweeen 0 and 1. e.g. LET a = RND
```

Functions
```
LEN(string) e.g. PRINT LEN("Hello") -> 5
VAL(string) e.g. PRINT VAL("1+2")
INT(number) e.g. INT(1.5)-> 1
STR$(number) e.g. STR$(2) -> "2"
LEFT$(string,n)
RIGHT$(string,n)
MID$(string,start,n)
PINREAD(pin) - see Arduino digitalRead()
ANALOGRD(pin) - see Arduino analogRead()
SIN(number) - Calculates sine of given angle in radians
COS(number) - Calculates cosine of given angle in radians
ATN(number) - Calculates arc tangent of value
SQRT(number) - Square root of value
LN(number) - Natural logarithm of value
EXP(number) - e^number
ABS(number) - Absolute value of number
MILLIS() - returns a running counter. Useful for timing purposes
ACOS - Inverse cosine of value
ASIN - Inverse sine of value
SGN - Sign function returns -1 for negative, 0 for 0 and +1 for positive input values
ATAN2(numbery,numberx) returns angle given coordinates on y and x axis
CHR$(number) - returns string with one character of the ascii value passed
ASC(number) - returns ascii code of first character of string passed
END - just terminates execution without giving a error messages as STOP does.
```

Limitations
--------------
The Exponent operator (^) works in the "wrong" direction if applied directly after eachother due to the left to right evaluation.
4^3^2 thus evaluates to 4096 instead of 262144 which is the mathematically correct order.

Will no longer work on low memory devices such as the arduino uno.
- Sketch is too big.
- I removed the progmem tags and reading from lower memory as that prevented it from running on the teensy.

Never tested on the USB serial but should work just fine if vtDriver is initialized with Serial instead of Serial1
