#pragma once

#include "host.h"


#define SCREEN_WIDTH        80
#define SCREEN_HEIGHT       24

#define EXTERNAL_EEPROM         0
#define EXTERNAL_EEPROM_ADDR    0x50    // I2C address (7 bits)
#define EXTERNAL_EEPROM_SIZE    32768   // only <=32k tested (64k might work?)

#define MAGIC_AUTORUN_NUMBER    0xFC

class SerialHost : public Host
{
public:
  void init(int buzzerPin);
  void sleep(long ms);
  void digitalWrite(int pin,int state);
  int digitalRead(int pin);
  int analogRead(int pin);
  void pinMode(int pin, int mode);
  void click();
  void startupTone();
  void cls();
  void showBuffer();
  void moveCursor(int x, int y);
  void outputString(const char *str);
  void outputChar(char c);
  void outputFloat(float f);
  char *floatToStr(float f, char *buf);
  void outputInt(long val);
  void newLine();
  char *readLine();
  char getKey();
  bool ESCPressed();
  void outputFreeMem(unsigned int val);
  void saveProgram(bool autoexec);
  void loadProgram();
  int getMillis();

  #if EXTERNAL_EEPROM
  #include <I2cMaster.h>
  void directoryExtEEPROM();
  bool saveExtEEPROM(char *fileName);
  bool loadExtEEPROM(char *fileName);
  bool removeExtEEPROM(char *fileName);
  #endif  
  
private:
  char * float2s(double f, unsigned int digits);


  #if EXTERNAL_EEPROM
  void writeExtEEPROM(unsigned int address, byte data);
  #endif
};

